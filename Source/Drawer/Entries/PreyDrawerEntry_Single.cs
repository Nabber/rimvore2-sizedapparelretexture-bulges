﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_Single : PreyDrawerEntry
    {
        List<BodyTypeDef> bodyTypes;
        List<ThingDef> races;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            if(records.Count != 1)
            {
                return "Not single prey";
            }
            Pawn prey = records.First().Prey;
            BodyTypeDef actualBodyType = prey.story?.bodyType;
            if(bodyTypes != null && !bodyTypes.Contains(actualBodyType))
            {
                return $"BodyType mismatch, needs one of: {bodyTypes.ToStringSafeEnumerable()}, is: {actualBodyType}";
            }
            if(races != null && !races.Contains(prey.def))
            {
                return $"Race mismatch, needs one of: {races.ToStringSafeEnumerable()}, is: {prey.def}";
            }
            return true;
        }
    }
}

﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_PreyCountAtDigestionProgress : PreyDrawerEntry
    {
        List<FloatRange> preyConfig = new List<FloatRange>();
        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            List<float> progressList = new List<float>();
            foreach (var record in records)
            {
                progressList.Add(DrawerRequestUtility.GetDigestionProgress(record));
            }

            var indicesToRemove = new List<int>();
            var usedConfigIndices = new List<int>();

            for (int i = 0; i < progressList.Count; i++)
            {
                var progress = progressList[i];
                var matched = false;

                for (int j = 0; j < preyConfig.Count; j++)
                {
                    var configRange = preyConfig[j];
                    if (configRange.Includes(progress) && !usedConfigIndices.Contains(j))
                    {
                        Log.Message($"Testing progress {progress} for matches against {configRange.min}~{configRange.max}: SUCCESS!");
                        indicesToRemove.Add(i);
                        usedConfigIndices.Add(j);
                        matched = true;
                        break;
                    }
                    else
                    {
                        Log.Message($"Testing progress {progress} for matches against {configRange.min}~{configRange.max}: FAILURE!");
                    }
                }

                if (!matched)
                {
                    Log.Message($"No matching range found for progress {progress}!");
                }
            }

            indicesToRemove.Reverse();
            foreach (var index in indicesToRemove)
            {
                progressList.RemoveAt(index);
            }

            if (progressList.Count > 0)
            {
                return $"Count: {progressList.Count}! Not all progressList values have corresponding ranges in preyConfig! Ignoring entry.";
            }

            return true;
        }


    }
}

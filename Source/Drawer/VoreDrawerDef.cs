﻿using RimVore2;
using RimWorld;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_SAR_Bulges
{
    public class VoreDrawerDef : Def
    {
        List<string> partNames;
        List<BodyTypeDef> predatorBodyTypes = new List<BodyTypeDef>();
        List<ThingDef> predatorRaces = new List<ThingDef>();
        public int priority = 1000;
        List<PreyDrawerEntry> drawerEntries;
        List<BodyPartGroupDef> hideIfAnyBodyPartGroupCovered = new List<BodyPartGroupDef>();

        public AcceptanceReport AppliesTo(DrawerDefRequest request)
        {
            if(!partNames.Contains(request.partName))
            {
                return $"part mismatch, needs: {string.Join(", ", partNames)}, is: {request.partName}";
            }
            BodyTypeDef actualPredatorBodyType = request.predator.story?.bodyType;
            if (predatorBodyTypes.Any() && !predatorBodyTypes.Contains(actualPredatorBodyType))
            {
                return $"predator bodytype mismatch, needs any of: {string.Join(", ", predatorBodyTypes.Select(d => d.defName))}, is: {actualPredatorBodyType}";
            }
            var actualPredatorRace = request.predator.def;
            if (predatorRaces.Any() && !predatorRaces.Contains(actualPredatorRace))
            {
                return $"predator race mismatch, needs any of: {string.Join(", ", predatorRaces.Select(d => d.defName))}, is: {actualPredatorRace}";
            }
            bool shouldCheckApparel = hideIfAnyBodyPartGroupCovered.Any() && request.predator.apparel != null;
            if (shouldCheckApparel)
            {
                foreach (BodyPartGroupDef bodyPartGroupDef in hideIfAnyBodyPartGroupCovered)
                {
                    if(request.predator.apparel.BodyPartGroupIsCovered(bodyPartGroupDef))
                    {
                        return $"body part group {bodyPartGroupDef.defName} is covered by apparel";
                    }
                }
            }
            return true;
        }

        public IEnumerable<GraphicData> GetGraphicsFor(List<VoreTrackerRecord> records)
        {
            foreach (PreyDrawerEntry entry in drawerEntries)
            {
                foreach (GraphicData graphic in entry.GetGraphics(records))
                {
                    yield return graphic;
                }
            }
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (partNames.NullOrEmpty())
                yield return $"List \"{nameof(partNames)}\" is null or empty";
            if (drawerEntries.NullOrEmpty())
                yield return $"List \"{nameof(drawerEntries)}\" is null or empty";
            else
            {
                foreach (string error in drawerEntries.SelectMany(key => key.ConfigErrors()))
                {
                    yield return error;
                }
            }
        }

    }
}

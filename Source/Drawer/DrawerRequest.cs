﻿using RimVore2;
using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RV2_SAR_Bulges
{
    public enum DrawerStageProgress
    {
        Intact = 10,
        Digest = 8,
        Churn = 6,
        Process = 4,
        Dump = 2
    }

    public struct DrawerDefRequest
    {
        public string partName;
        public Pawn predator;

        public override string ToString()
        {
            return $"(" +
                $"{nameof(partName)}={partName}, " +
                $"{nameof(predator)}={predator?.LabelShort?? "NULL"}, " +
                $")";
        }
    }
}

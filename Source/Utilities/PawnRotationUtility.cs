﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class PawnRotationUtility
    {
        public static Rot4 RotationStandingOrLaying(this Pawn pawn)
        {
            if(pawn.GetPosture() == PawnPosture.Standing)
            {
                return pawn.Rotation;
            }

            return pawn.Drawer.renderer.LayingFacing();
        }
    }
}

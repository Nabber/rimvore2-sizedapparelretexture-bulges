﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class DrawerTweakUtility
    {
        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawSize = false;
        [TweakValue("RV2SARBulges", 0, 2)]
        public static float drawSizeX = 1;
        [TweakValue("RV2SARBulges", 0, 2)]
        public static float drawSizeY = 1;

        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawOffset = false;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetX;
        [TweakValue("RV2SARBulges", -0.2f, 0.2f)]
        public static float drawOffsetY;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetZ;

        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawOffsetNorth = false;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetNorthX;
        [TweakValue("RV2SARBulges", -0.2f, 0.2f)]
        public static float drawOffsetNorthY;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetNorthZ;

        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawOffsetEast = false;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetEastX;
        [TweakValue("RV2SARBulges", -0.2f, 0.2f)]
        public static float drawOffsetEastY;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetEastZ;

        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawOffsetSouth = false;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetSouthX;
        [TweakValue("RV2SARBulges", -0.2f, 0.2f)]
        public static float drawOffsetSouthY;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetSouthZ;

        [TweakValue("RV2SARBulges", 0, 1)]
        public static bool overwriteDrawOffsetWest = false;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetWestX;
        [TweakValue("RV2SARBulges", -0.2f, 0.2f)]
        public static float drawOffsetWestY;
        [TweakValue("RV2SARBulges", -1, 1)]
        public static float drawOffsetWestZ;

        public static GraphicData CloneAndOverwriteOffsets(GraphicData graphicData)
        {
            GraphicData newData = new GraphicData();
            newData.CopyFrom(graphicData);
            if (overwriteDrawSize)
                newData.drawSize = new Vector2(drawSizeX, drawSizeY);
            if(overwriteDrawOffset)
                newData.drawOffset = new Vector3(drawOffsetX, drawOffsetY, drawOffsetZ);
            if(overwriteDrawOffsetNorth)
                newData.drawOffsetNorth = new Vector3(drawOffsetNorthX, drawOffsetNorthY, drawOffsetNorthZ);
            if(overwriteDrawOffsetEast)
                newData.drawOffsetEast = new Vector3(drawOffsetEastX, drawOffsetEastY, drawOffsetEastZ);
            if(overwriteDrawOffsetSouth)
                newData.drawOffsetSouth = new Vector3(drawOffsetSouthX, drawOffsetSouthY, drawOffsetSouthZ);
            if(overwriteDrawOffsetWest)
                newData.drawOffsetWest = new Vector3(drawOffsetWestX, drawOffsetWestY, drawOffsetWestZ);
            return newData;
        }

        public static int HashSum()
        {
            return HashUtility.CombineHashes(overwriteDrawOffset.GetHashCode(),
                drawSizeX, drawSizeY,
                overwriteDrawOffset, drawOffsetX, drawOffsetY, drawOffsetZ,
                overwriteDrawOffsetNorth, drawOffsetNorthX, drawOffsetNorthY, drawOffsetNorthZ,
                overwriteDrawOffsetEast, drawOffsetEastX, drawOffsetEastY, drawOffsetEastZ,
                overwriteDrawOffsetSouth, drawOffsetSouthX, drawOffsetSouthY, drawOffsetSouthZ,
                overwriteDrawOffsetWest, drawOffsetWestX, drawOffsetWestY, drawOffsetWestZ);
        }
    }
}

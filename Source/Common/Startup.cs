using HarmonyLib;
using RimVore2;
using Verse;

namespace RV2_SAR_Bulges
{
    [StaticConstructorOnStartup]
    internal static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony("Nabber.RV2SARBulges");
            harmony.PatchAll();

            RestoreGraphicsOnStartup();
        }

        private static void RestoreGraphicsOnStartup()
        {
            foreach (Pawn predator in GlobalVoreTrackerUtility.ActivePredators)
            {
                predator.GetBulgePawnData()?.drawer?.Recache();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using HarmonyLib;

namespace RV2_SAR_Bulges
{

    [HarmonyPatch(typeof(GraphicData), nameof(GraphicData.CopyFrom))]
    public static class Patch_GraphicData
    {
        /// <summary>
        /// Fixes base game bug in version 1.4.3901
        /// </summary>
        /// <param name="__instance"></param>
        [HarmonyPostfix]
        public static void FixWestOffset(ref GraphicData __instance, GraphicData other)
        {
            try
            {
                __instance.drawOffsetWest = other.drawOffsetWest;
            }
            catch(Exception e)
            {
                Log.Error($"Caught unhandled exception: {e}");
            }
        }
    }
}

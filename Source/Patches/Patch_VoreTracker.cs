﻿using HarmonyLib;
using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    [HarmonyPatch(typeof(VoreTracker), nameof(VoreTracker.SynchronizeHediffs))]
    public static class Patch_VoreTracker
    {
        [HarmonyPostfix]
        public static void RecacheVoreDrawer(Pawn ___pawn)
        {
            ___pawn.GetBulgePawnData()?.drawer?.Recache();
        }

        [DebugAction("RV2_Bulges", "Recache", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void DEBUG_Recache(Pawn p)
        {
            RecacheVoreDrawer(p);
        }
    }
}
